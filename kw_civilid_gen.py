import argparse
from typing import List

def get_check_digit(value) -> int:
    weight = (2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2)
    calculated_checksum = 0
    for i in range(11):
        calculated_checksum += int(value[i]) * weight[i]
    remainder = calculated_checksum % 11
    check_digit = 11 - remainder
    return check_digit


def obtain_date_details(datestr) -> List[str]:
    if '-' in datestr:
        return datestr.split('-')
    else:
        return datestr.split('/')


def kw_civil_id_gen(yyyy, mm, dd, mid) -> str:
    if str(yyyy).startswith("1"):
        cc = 2
    else:
        cc = 3
    res = f"{cc}{yyyy[:-2]}{mm}{dd}{mid}"
    return f"{res}{get_check_digit(res)}"

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("dob", help="yyyy/mm/dd or yyyy-mm-dd", type=str)
    parser.add_argument("mid", help="a string of four digits", type=str)

    args = parser.parse_args()

    return args


if __name__ == "__main__":
    args = parse_arguments()
    date = obtain_date_details(args.dob)
    kw_civil_id_gen(date[0], date[1], date[2] ,args.mid)

